const Course = require('../models/course')

//lets create a function that will allow us to add a new course inside the database.
module.exports.insert = (params) => {
	let course =  new Course({
		name: params.name,
		description: params.description,
		price: params.price
	})
	//once you have successfully captured the data inserted by the user via the request body, its now time to save it.
	return course.save().then((course, err) => {
		return (err) ? false : true
	})
}

//2/10 6:51pm

//we are going to create a new function that will display all of the courses that has active status of "true".

module.exports.getAll = () => {
	return Course.find({isActive: true}).then(courses => courses)
}

//we are going to create another function that will display a single course.

module.exports.get = (params) => {	
	return Course.findById(params.courseId).then(course => course)
}

// module.exports.subjectExists = (params) => {
// 	return User.find ({ name: params.name}).then(result => {
// 		return result.length > 0 ? true : false
// 	})
// }

//Task 2/15
module.exports.courseExists = (params) => {
    return Course.find({ name: params.name }).then(result => {
       return result.length > 0 ? true : false 
    })
  }