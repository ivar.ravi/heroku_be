//lets acquire the dependency needed to model our schema
const mongoose = require('mongoose')

//lets describe the anatomy of our document.
const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Course name is required']
	},
	description: {
		type: String,
		required: [true, 'Course description is required']
	},
	price: {
		type: Number,
		required: [true, 'Course price is required']
	},
	isActive: {
		type: Boolean, //boolean
		default: true
	}, //this will describe if the course is still available for enrollment
	createdOn: {
		type: Date,
		default: new Date()
	}, //this key will describe when the subject was added in the system.
	enrollees: [
		{
			userId: { //this will describe the id of the user to get the details.
				type: String,
				required: [true, 'user Id is required']
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}	//when the student enrolled for the subject.
		}
	]//this will describe/display the list of students that are enrolled for a specific subject.
})



module.exports =  mongoose.model('Course', courseSchema)