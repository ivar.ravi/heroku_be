const mongoose = require('mongoose')

//we are now going to create a schema or blueprint to our users

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		require: [true, 'firstName is required']
	},
	lastName: {
		type: String,
		require: [true, 'lastName is required']
	},
	email: {
		type: String,
		require: [true, 'email is required']
	},
	mobileNo: {
		type: String,
		require: [true, 'mobileNo is required']
	},
	password: {
		type: String,
		require: [true, 'password is required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	}, //this will be an array of objects, features a student can enroll more than one subject
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "course ID is required"]
			}, //this will identify the course or subject.
			enrolledOn: {
				type: Date,
				default: new Date()
			},	//this will describe the time and date when the student enrooled for the subject.
			status: {
				type: String,
				default: 'Enrolled' //alternative values (completed or canceled)
			} //this will describe if the student is accepted or rejected in the class.
		}
	]
})

//now that we have defined the structure of the schema, lets now create a model.

module.exports = mongoose.model('User', userSchema)