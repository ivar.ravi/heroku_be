const jwt = require('jsonwebtoken')
const secret = 'CourseBookingSystem' //we declared a secret word/phrase that will be used as signature/access token four our project
//the purpose of this module is that will hold all the script regarding to our app's authorization.

//why did we create a secret word?
//we created this secret word/access tokenn to verify the request coming from other apps.

//the purpose of this module is that it will hold all the script regarding to our app's authorization.

//lets create a function that will allow us to authorized the user using the access token.

module.exports.createAccessToken = (user) => {
	//we will identify the porps/keys of the user that we want to verify
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin//to identify the role to determine the restrictions
	}
	return jwt.sign(data, secret, {})
} //through the use of sign() we are creating a synchronouse signature with a default HMAC(hash-based message authentication code.)

//an access token can come in 2 forms?
// -> "opaque" string
//-> Json web token

//the reason behind creating/utilizing 2 access tokens.
//-> add another layer of security
//->verify if the access token came from the authorized/proper origin.
//-> pang add ng random factor sa pag generate ng access token na tayo lang ang may alam.

//lets create a function to verify wether the access token is correct.
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization //authorization is a request header, commonly used for HTTP basic authorization. it would set if the server requested authorization, and the browser then prompted the user for a username/password.

	//lets create a control structure to describe/identify whether the token will be allowed to pass.
	if(typeof token !== "undefined"){
		//if merong value na nakuha sa authorization property.
		token = token.slice(7, token.length) //the token will be a string data type.
		//first param - the number of characters that will be sliced off the string (token)
		//When the JWT is generated it will include 7 additional characters to include additonal security in the access token.
		return jwt.verify(token, secret, (err, data) => {
			return (err) ? res.send(err) : next()
			//next ay ginagamit sa middleware, invoking the next() it will let the middleware proceed to the next function.
		})
	}else{
		return res.send({ auth: "failed" });
	}
}

//we created the following functions.
//-> a function to generate an access token via JWT with our secret word.
//-> a function to verify if the access token came from the correct/proper origin/resource
//now, its time to decode or decrypt the access token since it is stull hashed.

//we separated the process inside each function to have a separation of concerns.
module.exports.decode = (token) => {
	//lets create a control structure to determine the response if an access token is captured.
	if(typeof token !== 'undefined'){
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			return (err) ? null : jwt.decode(token, {
				complete: true
			}).payload //payload is for options.
		})
	}else{
		return null //developers choice to determin the response ... null is when a data is properly identified but not given a value.
	}
}