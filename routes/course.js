const express = require('express')
const router = express.Router()
const CourseController = require('../controllers/course')
//lets acquire the dependencies needed.

//lets create a ENPOINT for our getall function which will allow us to display all of the courses available for enrollment.

router.get('/', (req, res) => {
	CourseController.getAll().then(courses => res.send(courses));
})

//lets create a route that will alloow us to send a request to create a new course.
router.post('/addCourse', (req, res) => {
	CourseController.insert(req.body).then(result => res.send(result));
})

//task is to create a route to get a single course from the db.
//the endpoint for this route will be a "placeholder parameter"
router.get('/:courseId', (req, res) => {
	const courseId = req.params.courseId//lets capture the input of the user
	CourseController.get({ courseId }).then(course => res.send(course))
})

//task  2/15
router.post('/course-exists', (req, res) => {
    //inside the body section lets describe what will be the procedure upon sending a request ib the route.
    CourseController.courseExists(req.body).then(result => res.send(result));
 })

module.exports = router;


