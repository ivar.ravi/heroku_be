let express = require('express')
let mongoose = require('mongoose')
let app = express()
require('dotenv').config()
const cors = require('cors')
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true}))

let port = process.env.PORT;
let connectionString = process.env.DB_CONNECTION_STRING;

mongoose.connect(connectionString,{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', () => console.log(`Now connected to mongoDB Atlas`)); //terminal

app.get('/', (req, res) => {
	res.send("successfully hosted")
})

app.listen(process.env.PORT || 4000, () => console.log (`server up and running on port ${port}`)); //terminal //port

//we need to define the routes
const userRoutes = require('./routes/user')
const courseRoutes = require('./routes/course')

app.use('/api/users', userRoutes)
app.use('/api/courses', courseRoutes)